


import 'package:appoperativamkm/sidebar/sidebar_layout.dart';
import 'package:appoperativamkm/src/pages/checklist_page.dart';
import 'package:appoperativamkm/src/pages/login_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> getAppRoutes() {
    return <String, WidgetBuilder>{
    '/': (BuildContext context) =>  SideBarLayout(),
    'home': (BuildContext context) => ChecklistPage(),
    'login' : (BuildContext context) => LoginPage(),
    };

}