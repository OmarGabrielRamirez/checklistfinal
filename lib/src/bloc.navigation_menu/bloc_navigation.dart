
import 'package:appoperativamkm/src/pages/checklist_page.dart';
import 'package:bloc/bloc.dart';

enum NavigationEvents { ChecklistPageClickedEvent}

abstract class NavigationStates {}

class NavigationBloc extends Bloc<NavigationEvents, NavigationStates> {
  @override
  NavigationStates get initialState => ChecklistPage();

  @override
  Stream<NavigationStates> mapEventToState(NavigationEvents event ) async* {
    switch (event) {
      case NavigationEvents.ChecklistPageClickedEvent:
        yield ChecklistPage();
        break;
    }
  }
}
