import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ViewResultsCheckListPage extends StatefulWidget {
  int idCheck;
  String idTypeCheck;
  int ptjFinally;

  ViewResultsCheckListPage(
    this.idCheck,
    this.idTypeCheck,
    this.ptjFinally,
  );

  @override
  ViewResultsCheckListPageState createState() =>
      ViewResultsCheckListPageState();
}

class ViewResultsCheckListPageState extends State<ViewResultsCheckListPage> {
  int _idChec, _ptjFinally, _ptjDes, ptjCalGood, ptjCalBad,puntajeByBd;
  double _percentageBad, _percentageGood, sizeFontTb = 14.0;
  String _idTypeCheck = "";

  setContainerIconPuntaje(
      double _percentageBad, double _percentageGood, int _puntajeTotal) {
    if (_puntajeTotal <= _percentageBad) {
      return Container(
        padding: EdgeInsets.only(top: 40),
        child: Column(children: <Widget>[
          Container(
            child: Icon(
              Icons.close,
              size: 60,
              color: Colors.red,
            ),
          ),
          Container(
            child: Text(
              "Deficiente",
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontSize: 17,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ]),
      );
    } else if (_puntajeTotal > _percentageBad &&
        _puntajeTotal < _percentageGood) {
      return Container(
        padding: EdgeInsets.only(top: 40),
        child: Column(children: <Widget>[
          Container(
            child: Icon(
              Icons.check,
              size: 60,
              color: Colors.amberAccent,
            ),
          ),
          Container(
            child: Text(
              "Regular",
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontSize: 17,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ]),
      );
    } else if (_puntajeTotal > _percentageGood) {
      return Container(
        padding: EdgeInsets.only(top: 40),
        child: Column(children: <Widget>[
          Container(
            child: Icon(
              Icons.check,
              size: 60,
              color: Colors.green,
            ),
          ),
          Container(
            child: Text(
              "Bueno",
              style: TextStyle(
                  fontFamily: 'Lato',
                  fontSize: 17,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ]),
      );
    }
  }

  getPuntaje(String _idTypeCheck) async {
    Map data = {
      'idTypeCheck': _idTypeCheck,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/datachecksbyid", body: data);
    var dataSub = json.decode(response.body);

    setState(() {
      puntajeByBd = int.parse(dataSub[0]['puntaje_total']);
      ptjCalGood = dataSub[0]['cal_b'];
      ptjCalBad = dataSub[0]['cal_m'];
    });

    convertToNumber(ptjCalGood, ptjCalBad, puntajeByBd);
  }

  convertToNumber(int ptjGood, int ptjBad, int ptjTotal) async {
    setState(() {
      _percentageGood = ptjTotal * double.parse("." + ptjGood.toString());
      _percentageBad = ptjTotal * double.parse("." + ptjBad.toString());
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      puntajeByBd= 0;
      _percentageBad = 0.0;
      _percentageGood = 0.0;
      _idChec = widget.idCheck;
      _ptjFinally = widget.ptjFinally;
      _idTypeCheck = widget.idTypeCheck;
    });
    getPuntaje(_idTypeCheck);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "#" + _idChec.toString() + " Detalles - Puntaje",
          style: TextStyle(
              fontFamily: 'Lato',
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: Colors.white),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
        width: MediaQuery.of(context).size.width - 40,
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                child: DataTable(
                  columns: [
                    DataColumn(
                      numeric: true,
                      label: Container(
                          alignment: Alignment.center,
                          child: Text(
                            "Concepto",
                            style: TextStyle(
                                fontFamily: 'Lato',
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                    DataColumn(
                        label: Text(
                          "Puntaje",
                          style: TextStyle(
                              fontFamily: 'Lato',
                              fontSize: 18,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                        numeric: true),
                  ],
                  rows: [
                    DataRow(cells: [
                      DataCell(Text(
                        "Posibles",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: sizeFontTb,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      )),
                      DataCell(Text(
                       puntajeByBd.toString(),
                        style: TextStyle(
                            fontSize: sizeFontTb,
                            color: Colors.black,
                            fontWeight: FontWeight.w300),
                      )),
                    ]),
                    DataRow(cells: [
                      DataCell(Text(
                        "Obtenidos",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: sizeFontTb,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      )),
                      DataCell(Text(
                        _ptjFinally.toString(),
                        style: TextStyle(
                            fontSize: sizeFontTb,
                            color: Colors.black,
                            fontWeight: FontWeight.w300),
                      )),
                    ]),
                    DataRow(cells: [
                      DataCell(Text(
                        "Faltantes  ",
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: sizeFontTb,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      )),
                      DataCell(Text(
                        (puntajeByBd - _ptjFinally).toString(),
                        style: TextStyle(
                            fontSize: sizeFontTb,
                            color: Colors.black,
                            fontWeight: FontWeight.w300),
                      )),
                    ]),
                  ],
                ),
              ),
              setContainerIconPuntaje(
                  _percentageBad, _percentageGood, _ptjFinally),
              Container(
                width: MediaQuery.of(context).size.width - 40,
                padding: EdgeInsets.only(top: 50, left: 40, right: 40),
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).popAndPushNamed('/');
                  },
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.red[600],
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Center(
                      child: Text(
                        'Terminar',
                        style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 15.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
