import 'package:appoperativamkm/src/pages/view_sub_check_page.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ShowCheckListPage extends StatefulWidget {
  int idCheck;
  String idTypeCheck;
  String nameTypeCheck;

  ShowCheckListPage(this.idCheck, this.nameTypeCheck, this.idTypeCheck);

  @override
  ShowCheckListPageState createState() => ShowCheckListPageState();
}

class ShowCheckListPageState extends State<ShowCheckListPage> {
  int _idCheck,
      totalPages,
      _ptjFin,
      _ptjDes,
      puntajeByBd,
      ptjCalGood,
      ptjCalBad;
  String _idTypeCheck, _nameTypeCheck, primerSect;
  double _percentageBad, _percentageGood;
  List<SubCheck> subchecks = [];

  setContainerIconPuntaje(
      double _percentageBad, double _percentageGood, int _puntajeTotal) {
    if (_puntajeTotal <= _percentageBad) {
      return Container(
        child: Icon(Icons.close, size: 17, color: Colors.red),
      );
    } else if (_puntajeTotal > _percentageBad &&
        _puntajeTotal < _percentageGood) {
      return Container(
        child: Icon(Icons.check,size: 17, color: Colors.amberAccent),
      );
    } else if (_puntajeTotal > _percentageGood) {
      return Container(
        child: Icon(Icons.check, size: 17, color: Colors.green),
      );
    }
  }

  Future<List<CheckList>> getDataCheck() async {
    Map data = {'idCheckList': _idCheck.toString()};
    var response = await http
        .post("https://intranet.prigo.com.mx/api/getdatacheck", body: data);
    var dataSub = json.decode(response.body);
    List<CheckList> checks = [];
    for (var x in dataSub) {
      CheckList check = CheckList(
          x['idCheckList'],
          x['responsable'],
          x['fechaGenerada'],
          x['nombreSuc'],
          x['puntajeFinal'],
          x['puntajeDeseado']);
      checks.add(check);
    }
    return checks;
  }

  getDataSubs(String _idTypeCheck) async {
    Map data = {
      'idTypeCheck': _idTypeCheck,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/subdatachecksids", body: data);
    var dataSub = json.decode(response.body);
    for (var x in dataSub) {
      SubCheck sub = SubCheck(x['idSub']);
      subchecks.add(sub);
    }
    setState(() {
      totalPages = subchecks.length;
      primerSect = subchecks[0].idSub;
    });
    getPuntaje(_idTypeCheck);
  }

  getPuntaje(String _idTypeCheck) async {
    Map data = {
      'idTypeCheck': _idTypeCheck,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/datachecksbyid", body: data);
    var dataSub = json.decode(response.body);

    setState(() {
      puntajeByBd = int.parse(dataSub[0]['puntaje_total']);
      ptjCalGood = dataSub[0]['cal_b'];
      ptjCalBad = dataSub[0]['cal_m'];
    });

    convertToNumber(ptjCalGood, ptjCalBad, puntajeByBd);
  }

  convertToNumber(int ptjGood, int ptjBad, int ptjTotal) async {
    setState(() {
      _percentageGood = ptjTotal * double.parse("." + ptjGood.toString());
      _percentageBad = ptjTotal * double.parse("." + ptjBad.toString());
    });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      puntajeByBd = 0;
      _percentageBad = 0.0;
      _percentageGood = 0.0;
      _idCheck = widget.idCheck;
      _nameTypeCheck = widget.nameTypeCheck;
      _idTypeCheck = widget.idTypeCheck;
    });
    getDataSubs(_idTypeCheck);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blueGrey[900],
        child: FutureBuilder(
            future: getDataCheck(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.data == null) {
                return Container(
                  margin:
                      EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
                  width: MediaQuery.of(context).size.width - 40,
                  child: Card(
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SpinKitCircle(
                          color: Colors.black,
                          size: 40.0,
                        ),
                        JumpingText(
                          'Cargando...',
                          style: TextStyle(
                              fontFamily: 'Lato',
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                );
              } else {
                _ptjFin = snapshot.data[0].puntajeFinal;
                _ptjDes = int.parse(snapshot.data[0].puntajeDeseado);
                return Container(
                  margin:
                      EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
                  width: MediaQuery.of(context).size.width - 40,
                  child: Card(
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: ListView(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 40),
                          child: Column(children: <Widget>[
                            Container(
                              child: Align(
                                alignment: Alignment.center,
                                child: Container(
                                  child: Text(
                                      "Checklist" + " #" + _idCheck.toString(),
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: 'Lato',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ),
                              ),
                            ),
                            Container(
                              // padding: EdgeInsets.only(bottom: 10),
                              child: Align(
                                alignment: Alignment.center,
                                child: Container(
                                  child: Text(
                                    _nameTypeCheck,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: 'Lato',
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black45),
                                  ),
                                ),
                              ),
                            ),
                          ]),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 65, right: 65),
                          child: Column(children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 15, bottom: 15),
                              child: Align(
                                alignment: Alignment.center,
                                child: Container(
                                  child: Text("Fecha / Hora de visita ",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: 'Lato',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ),
                              ),
                            ),
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                border: Border.all(width: 0.8),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.date_range,
                                    size: 18,
                                    color: Colors.red,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                      snapshot.data[0].fecha
                                          .substring(0, 18 - 2),
                                      style: TextStyle(
                                          fontSize: 13,
                                          fontFamily: 'Lato',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black))
                                ],
                              ),
                            )
                          ]),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 65, right: 65),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 15, bottom: 15),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      child: Text("Sucursal ",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: 'Lato',
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15.0),
                                    border: Border.all(width: 0.8),
                                  ),
                                  child: Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.store,
                                            size: 18, color: Colors.red),
                                        SizedBox(width: 5),
                                        Text(
                                          snapshot.data[0].nombreSuc,
                                          style: TextStyle(
                                              fontFamily: 'Lato',
                                              fontSize: 13,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(width: 10),
                                      ],
                                    ),
                                  ),
                                )
                              ]),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 65, right: 65),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 15, bottom: 15),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      child: Text("Puntaje ",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontFamily: 'Lato',
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 40,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15.0),
                                    border: Border.all(width: 0.8),
                                  ),
                                  child: Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        setContainerIconPuntaje(
                                            _percentageBad,
                                            _percentageGood,
                                            snapshot.data[0].puntajeFinal),
                                        SizedBox(width: 5),
                                        Text(
                                          snapshot.data[0].puntajeFinal
                                                  .toString() +
                                              " / " +
                                              snapshot.data[0].puntajeDeseado,
                                          style: TextStyle(
                                              fontFamily: 'Lato',
                                              fontSize: 13,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(width: 10),
                                      ],
                                    ),
                                  ),
                                )
                              ]),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 45, right: 45),
                          child: Column(children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 15, bottom: 15),
                              child: Align(
                                alignment: Alignment.center,
                                child: Container(
                                  child: Text(" Gerente / Responsable ",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: 'Lato',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black)),
                                ),
                              ),
                            ),
                            Container(
                              height: 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                border: Border.all(width: 0.8),
                              ),
                              child: Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.perm_identity,
                                        size: 18, color: Colors.red),
                                    SizedBox(width: 10),
                                    Text(
                                      snapshot.data[0].responsable,
                                      style: TextStyle(
                                          fontFamily: 'Lato',
                                          fontSize: 13,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                    SizedBox(width: 5),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width - 40,
                              padding: EdgeInsets.only(top: 40),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) => ViewSubCheckPage(
                                          _idTypeCheck,
                                          _idCheck,
                                          primerSect,
                                          0,
                                          totalPages - 1,
                                          _ptjFin,
                                          _ptjDes),
                                    ),
                                  );
                                },
                                child: Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                      color: Colors.red[600],
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: Center(
                                    child: Text(
                                      'Ver detalles',
                                      style: TextStyle(
                                          fontFamily: 'Lato',
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.w300,
                                          color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ]),
                        )
                      ],
                    ),
                  ),
                );
              }
            }),
      ),
      appBar: AppBar(
        title: Text(
          "#" + _idCheck.toString() + " Resultados - " + _nameTypeCheck,
          style: TextStyle(
            fontFamily: 'Lato',
            fontWeight: FontWeight.w500,
            fontSize: 14,
          ),
        ),
      ),
    );
  }
}

class CheckList {
  int idCheckList;
  String responsable;
  String fecha;
  String nombreSuc;
  int puntajeFinal;
  String puntajeDeseado;

  CheckList(this.idCheckList, this.responsable, this.fecha, this.nombreSuc,
      this.puntajeFinal, this.puntajeDeseado);
}

class SubCheck {
  String idSub;
  SubCheck(this.idSub);
}
