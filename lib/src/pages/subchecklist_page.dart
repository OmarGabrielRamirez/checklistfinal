import 'dart:convert';

import 'package:progress_indicators/progress_indicators.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SubChecklistTotal extends StatefulWidget {
  String idTypeCheck;
  String puntaje;
  String name;

  SubChecklistTotal(this.idTypeCheck, this.name, this.puntaje);

  @override
  SubChecklistTotalState createState() => SubChecklistTotalState();
}

class SubChecklistTotalState extends State<SubChecklistTotal> {
  String _idTypeCheck;
  bool _isChecked = false;
  String _puntaje;
  String _name;
  int totalPuntaje = 0;
  var itemsStatus = List<bool>();
  List<Item> globalItems = [];

  @override
  void initState() {
    super.initState();
    setState(() {
      _idTypeCheck = widget.idTypeCheck;
      _name = widget.name;
      _puntaje = widget.puntaje;
    });
  }

    void checkboxChecked(bool val) {
//    setState(() {
    _isChecked = val;
//    });
    print('clliccked' + val.toString());
  }

   setIcon(int indicator) {
    if (indicator == 1) {
      return Icon(
        Icons.assistant_photo,
        size: 17,
        color: Colors.red,
      );
    } else if (indicator == 0) {
      return Icon(
        Icons.assistant_photo,
        size: 17,
        color: Colors.blueGrey[900],
      );
    }
  }


  getPuntaje(bool val, int ptj)async{
    if(val == true){ 
      int totalP = 0;
      setState(() {
        totalP +=ptj;
        totalP = totalPuntaje;
      });
      

    }else if(val == false){


    }
  }

  bool parseString(String value){
    bool status;
    if(value =="true"){
      status = true;
    }else if(value=="false"){
      status = false;
    }
   return status;
  }



  Future<List<SubCheckList>> getDataSubSecc() async {
    Map data = {
      'idTypeCheck': _idTypeCheck,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/subdatachecks", body: data);
    var dataSub = json.decode(response.body);
    List<SubCheckList> subchecks = [];
    for (var x in dataSub) {
      SubCheckList sub = SubCheckList(x['idSub'], x['nombre']);
      subchecks.add(sub);
    }
    return subchecks;
  }

  Future<List<Item>> getDataItems(String idSub) async {
    Map data = {
      'idSub': idSub,
    };
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/getitemschecks",
        body: data,
        headers: {"Accept": "application/json"});
    var itemsData = json.decode(response.body);
    List<Item> items = [];
    for (var i in itemsData) {
      Item item = Item(i["idItem"], i["accion"], i["critico"], i["puntaje"], i["edo"]);
      items.add(item);
      itemsStatus.add(false);
    }

  globalItems.addAll(items);

  return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.blueGrey[900],
      appBar: AppBar(
        actions: <Widget>[
          IconButton(icon:  Icon(Icons.check ), onPressed: (){
            
          }),
          SizedBox(width:5)
        ],
        centerTitle: true,
          title: Text(
            _name,
            style: TextStyle(
                fontFamily: 'Lato',
                fontWeight: FontWeight.w500,
                fontSize: 15,
                color: Colors.white),
          ),
          backgroundColor: Colors.red),
      body: Container(
        child: FutureBuilder(
            future: getDataSubSecc(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.data == null) {
                return Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SpinKitCircle(
                        color: Colors.white,
                        size: 40.0,
                      ),
                      JumpingText(
                        'Cargando...',
                        style: TextStyle(
                            fontFamily: 'Lato',
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      ),
                    ],
                  ),
                );
              } else {
                return Container(
                  // margin:
                  //     EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
                  //     child: PageView.builder(
                  //       scrollDirection: Axis.horizontal,
                  //       itemCount: snapshot.data.length,
                  //       itemBuilder: (BuildContext context, int index) {
                        
                  //       })
                  // child: Scrollbar(
                  //   child: ListView.builder(
                  //       shrinkWrap: true,
                  //       scrollDirection: Axis.horizontal,
                  //       itemCount: snapshot.data.length,
                  //       itemBuilder: (BuildContext context, int index) {
                  //         return Container(
                  //           width: MediaQuery.of(context).size.width - 40,
                  //           child: Card(
                  //             shape: RoundedRectangleBorder(
                  //               borderRadius: BorderRadius.circular(15.0),
                  //             ),
                  //             child: Column(
                  //               children: <Widget>[
                  //                 Container(
                  //                   child: Text(totalPuntaje.toString()),
                  //                 ),
                  //                 Container(
                  //                   padding:
                  //                       EdgeInsets.only(top: 20, bottom: 20),
                  //                   child: Text(
                  //                     snapshot.data[index].nombre,
                  //                     style: TextStyle(
                  //                         fontFamily: 'Lato',
                  //                         fontWeight: FontWeight.bold),
                  //                   ),
                  //                 ),
                  //                 // Container(
                  //                 //   child: FutureBuilder(
                  //                 //       future: getDataItems(
                  //                 //     'VITEXT'),
                  //                 //       builder: (BuildContext context,
                  //                 //           AsyncSnapshot snapshot) {
                  //                 //         if (snapshot.data == null) {
                  //                 //           return Container(
                  //                 //             child: Column(
                  //                 //               mainAxisAlignment:
                  //                 //                   MainAxisAlignment.center,
                  //                 //               children: <Widget>[
                  //                 //                 SpinKitCircle(
                  //                 //                   color: Colors.white,
                  //                 //                   size: 40.0,
                  //                 //                 ),
                  //                 //                 JumpingText(
                  //                 //                   'Cargando...',
                  //                 //                   style: TextStyle(
                  //                 //                       fontFamily: 'Lato',
                  //                 //                       color: Colors.white,
                  //                 //                       fontWeight:
                  //                 //                           FontWeight.bold,
                  //                 //                       fontSize: 12),
                  //                 //                 ),
                  //                 //               ],
                  //                 //             ),
                  //                 //           );
                  //                 //         } else {
                  //                 //           return Expanded(
                  //                 //             child: ListView.builder(
                  //                 //                 padding: EdgeInsets.only(
                  //                 //                     left: 10,
                  //                 //                     right: 5,
                  //                 //                     bottom: 10),
                  //                 //                 itemCount:
                  //                 //                     snapshot.data.length,
                  //                 //                 itemBuilder:
                  //                 //                     (BuildContext context,
                  //                 //                         int index) {
                                                          
                  //                 //                   return CheckboxListTile(
                  //                 //                       title: Text(
                  //                 //                         snapshot.data[index]
                  //                 //                             .descripcion,
                  //                 //                         style: TextStyle(
                  //                 //                             fontFamily:
                  //                 //                                 'Lato',
                  //                 //                             fontSize: 15,
                  //                 //                             fontWeight:
                  //                 //                                 FontWeight
                  //                 //                                     .bold,
                  //                 //                             color:
                  //                 //                                 Colors.black),
                  //                 //                       ),
                  //                 //                       dense: true,
                  //                 //                       controlAffinity:
                  //                 //                           ListTileControlAffinity
                  //                 //                               .platform,
                  //                 //                       secondary: setIcon(
                  //                 //                           snapshot.data[index]
                  //                 //                               .critico),
                  //                 //                       value:
                  //                 //                           parseString(snapshot.data[index].status),
                  //                 //                       onChanged: (bool val) {
                  //                 //                             if(val == true){
                  //                 //                           setState(() {
                  //                 //                               totalPuntaje +=snapshot.data[index].puntaje;
                  //                 //                           });
                  //                 //                     }else if(val == false){
                  //                 //                           setState(() {
                  //                 //                               totalPuntaje -=snapshot.data[index].puntaje;
                  //                 //                           });
                  //                 //                     }
                  //                 //                         setState(() {
                  //                 //                           itemsStatus[index] =
                  //                 //                               !itemsStatus[
                  //                 //                                   index];
                  //                 //                         });
                  //                 //                       });
                  //                 //                   // return Container(
                  //                 //                   //   child: Row(
                  //                 //                   //     children:<Widget>[
                  //                 //                   //       Container(
                  //                 //                   //         child:setIcon(snapshot
                  //                 //                   //   .data[index].critico),
                  //                 //                   //       )
                  //                 //                   //     ]
                  //                 //                   //   ),
                  //                 //                   // );
                  //                 //                   // return ListTile(
                  //                 //                   //   leading: Checkbox(
                  //                 //                   //       value: itemsStatus[
                  //                 //                   //           index],
                  //                 //                   //       onChanged:
                  //                 //                   //           (bool val) {
                  //                 //                   //         if (val == true) {
                  //                 //                   //           // item++;
                  //                 //                   //           if (snapshot
                  //                 //                   //                   .data[
                  //                 //                   //                       index]
                  //                 //                   //                   .critico ==
                  //                 //                   //               1) {
                  //                 //                   //             // itemsCriticos--;
                  //                 //                   //           } else if (snapshot
                  //                 //                   //                   .data[
                  //                 //                   //                       index]
                  //                 //                   //                   .critico ==
                  //                 //                   //               0) {
                  //                 //                   //             // itemsNormales--;
                  //                 //                   //           }
                  //                 //                   //         } else if (val ==
                  //                 //                   //             false) {
                  //                 //                   //           // item--;
                  //                 //                   //           if (snapshot
                  //                 //                   //                   .data[
                  //                 //                   //                       index]
                  //                 //                   //                   .critico ==
                  //                 //                   //               1) {
                  //                 //                   //             // itemsCriticos++;
                  //                 //                   //           } else if (snapshot
                  //                 //                   //                   .data[
                  //                 //                   //                       index]
                  //                 //                   //                   .critico ==
                  //                 //                   //               0) {
                  //                 //                   //             // itemsNormales++;
                  //                 //                   //           }
                  //                 //                   //         }
                  //                 //                   //         setState(() {
                  //                 //                   //           // valCheck.update(
                  //                 //                   //           //     '"' +
                  //                 //                   //           //         snapshot.data[index].index
                  //                 //                   //           //             .toString() +
                  //                 //                   //           //         '"',
                  //                 //                   //           //     (existingValue) => val);
                  //                 //                   //           // mapValues = json.encode(valCheck.toString());
                  //                 //                   //             itemsStatus[
                  //                 //                   //                     index] =
                  //                 //                   //                 !itemsStatus[
                  //                 //                   //                     index];
                  //                 //                   //         });
                  //                 //                   //       }),
                  //                 //                   //   title: Text(
                  //                 //                   //     snapshot.data[index]
                  //                 //                   //         .descripcion,
                  //                 //                   //     style: TextStyle(
                  //                 //                   //         fontFamily: 'Lato',
                  //                 //                   //         fontSize: 15,
                  //                 //                   //         fontWeight:
                  //                 //                   //             FontWeight.bold,
                  //                 //                   //         color:
                  //                 //                   //             Colors.black),
                  //                 //                   //   ),
                  //                 //                   //   trailing: setIcon(snapshot.data[index]
                  //                 //                   //         .critico)
                  //                 //                   // );
                  //                 //                 }),
                  //                 //           );
                  //                 //         }
                  //                 //       }),
                  //                 // )
                  //               ],
                  //             ),
                  //           ),
                  //         );
                  //       }),
                  // ),
                );
              }
            }),
      ),
    );
  }
}

class SubCheckList {
  final String id;
  final String nombre;

  SubCheckList(this.id, this.nombre);
}

class Item {
  final int index;
  final String descripcion;
  final int critico;
  final int puntaje;
  final String status;

  Item(this.index, this.descripcion, this.critico, this.puntaje, this.status);
}
