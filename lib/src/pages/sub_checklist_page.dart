import 'dart:io';

import 'package:appoperativamkm/src/pages/checklist_results_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:status_alert/status_alert.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:photo_view/photo_view.dart';
import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'dart:convert';

class SubChecklistPage extends StatefulWidget {
  String idTypeCheck;
  String idCheckActually;
  String idSub;
  int numberPage;
  int totalPages;
  int puntajeAct;

  SubChecklistPage(this.idTypeCheck, this.idCheckActually, this.idSub,
      this.numberPage, this.totalPages, this.puntajeAct);

  @override
  SubChecklistPageState createState() => SubChecklistPageState();
}

class SubChecklistPageState extends State<SubChecklistPage> {
  final _comentariosController = TextEditingController();
  String mapValues;
  double porcert;
  bool _imgNecessary;
  int _numberPage, _totalPages, _totalPuntaje = 0, selectedIndex = 0;
  int totalPhotos =0;
  String _idTypeCheck,
      base64Image,
      _nombre = "",
      _sect = "",
      _indicationsImg = "",
      _statusUploaImage = "noexecute",
      _idCheckActually = "",
      _nameImageSuccess = "";

  File _image;
  List<SubCheck> subchecks = [];
  List valuesCheck = [];
  List<Item> items = [];
  List imagesListTotal = List<ItemImage>();
  var itemsStatus = List<bool>();
  final valCheck = Map<String, bool>();

  getName(String _sect) async {
    Map data = {
      'idTypeCheck': _sect,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/subdatachecks", body: data);
    var dataSub = json.decode(response.body);

    setState(() {
      _nombre = dataSub[0]['nombre'];
    });
  }

  Future _getImage(File _img) async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);
    setState(() {
      base64Image = base64Encode(image.readAsBytesSync());
    });
    setState(() {
      _image = image;
      _statusUploaImage = "pick_image";
    });
  }

  Future getPhoto(ItemImage itemimg, int index) async {
    try {
      var imageAct = await ImagePicker.pickImage(
          source: ImageSource.camera, imageQuality: 50);
      var base64ImageAct = base64Encode(
        imageAct.readAsBytesSync(),
      );

      setState(() {
        imagesListTotal[index] = ItemImage(
            image64: base64ImageAct,
            image: imageAct,
            indexName: itemimg.indexName,
            status: 'pick_image');
      });
    } catch (e) {
      StatusAlert.show(
        context,
        blurPower: 10,
        titleOptions: StatusAlertTextConfiguration(
          style: TextStyle(
            fontFamily: 'Lato',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        duration: Duration(seconds: 2),
        title: "Toma la fotografia indicada",
        configuration: IconConfiguration(icon: AntDesign.warning),
      );
    }
  }

  startUploadImage(String nameFile, File image, String filepst) {
    setState(() {
      _statusUploaImage = "loading_upload";
    });
    String fileNameImage_ = image.path.split("/").last;
    upload(fileNameImage_, nameFile, filepst);
  }

  upload(String fileNameImage, String nameFile, String filepost) async {
    var response = await http
        .post("https://intranet.prigo.com.mx/storage/check/upload.php", body: {
      "image": base64Image,
      "name": _idCheckActually + "-" + _idTypeCheck + "-" + _sect + ".jpg",
    });

    if (response.statusCode == 200) {
      setState(() {
        _nameImageSuccess =
            _idCheckActually + "-" + _idTypeCheck + "-" + _sect + ".jpg";
        _statusUploaImage = "success_upload";
      });
    } else {
      _statusUploaImage = "error_upload";
    }
  }

  uploadFile(ItemImage itmimg, int index) async {
    setState(() {
      imagesListTotal[index] = ItemImage(
          image64: itmimg.image64,
          image: itmimg.image,
          indexName: itmimg.indexName,
          status: 'loading_upload');
    });
    var response = await http
        .post("https://intranet.prigo.com.mx/storage/check/upload.php", body: {
      "image": itmimg.image64,
      "name": _idCheckActually +
          "-" +
          _idTypeCheck +
          "-" +
          _sect +
          "-" +
          itmimg.indexName +
          ".jpg",
    });

    if (response.statusCode == 200) {
      setState(() {
        imagesListTotal[index] = ItemImage(
            image64: itmimg.image64,
            image: itmimg.image,
            indexName: itmimg.indexName,
            status: 'success_upload');
      });
    } else {
      setState(() {
        imagesListTotal[index] = ItemImage(
            image64: itmimg.image64,
            image: itmimg.image,
            indexName: itmimg.indexName,
            status: 'error_upload');
      });
    }
  }

  getDataSubs(String _idTypeCheck, int numberP, int totalPages) async {
    Map data = {
      'idTypeCheck': _idTypeCheck,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/subdatachecksids", body: data);
    var dataSub = json.decode(response.body);
    for (var x in dataSub) {
      SubCheck sub = SubCheck(x['idSub']);
      subchecks.add(sub);
    }
    setState(() {
      if (numberP == 0) {
        _sect = subchecks[0].idSub;
      } else if (numberP <= totalPages || numberP != totalPages) {
        _sect = subchecks[numberP].idSub;
      } else {
        _sect = "No data";
      }
    });
    getName(_sect);
    getDataItems(_sect);
    getDataImage(_sect);
  }

  getDataImage(String _sect) async {
    Map data = {
      'idTypeCheck': _sect,
    };

    var response = await http
        .post("https://intranet.prigo.com.mx/api/getimagecheck", body: data);
    if (response.statusCode == 200) {
      var dataSub = json.decode(response.body);
      var _list = dataSub.values.toList();

      switch (_list[0]) {
        case 0:
          setState(() {
            _imgNecessary = false;
            _nameImageSuccess = "image_no_required";
          });
          break;
        case 1:
          var imagesTotal = new List<ItemImage>(_list[2]);
          List imagesList = List<ItemImage>();
          var counter = 0;
          for (var x in imagesTotal) {
            counter++;
            ItemImage itemImage = ItemImage(
                indexName: counter.toString(),
                image64: null,
                image: null,
                status: 'noexecute');
            imagesList.add(itemImage);
          }

          setState(() {
            totalPhotos =imagesList.length;
            imagesListTotal = imagesList;
            _imgNecessary = true;
            _indicationsImg = _list[1];
          });
          print(totalPhotos);
          break;
      }
    } else {}
  }

  getDataItems(String idSub) async {
    Map data = {
      'idSub': idSub,
    };
    var response = await http.post(
        "https://intranet.prigo.com.mx/api/getitemschecks",
        body: data,
        headers: {"Accept": "application/json"});
    var itemsData = json.decode(response.body);
    for (var i in itemsData) {
      Item item =
          Item(i["idItem"], i["accion"], i["critico"], i["puntaje"], i["edo"]);
      setState(() {
        items.add(item);
        valCheck.putIfAbsent('"' + i["idItem"].toString() + '"', () => false);
      });

      itemsStatus.add(false);
    }

    return items;
  }

  saveInfoCheckDetail() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String idTypeCheckList = sharedPreferences.getString('idTypeCheck');
    String responsable = sharedPreferences.getString('gerente');
    String idSucursal = sharedPreferences.getString('idSuc');
    String fechaGenerada = sharedPreferences.getString('dateAuto');
    String fechaIngresada = sharedPreferences.getString('dateUser');

    Map data = {
      'idTipoCheckList': idTypeCheckList,
      'responsable': responsable,
      'idSuc': idSucursal,
      'fechaGenerada': fechaGenerada,
      'fechaIngresada': fechaIngresada,
      'puntajeFinal': _totalPuntaje.toString()
    };

    Map<String, String> headers = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
    };

    var response = await http.post(
        "https://intranet.prigo.com.mx/api/guardarcheckinfo",
        body: data,
        headers: headers);

    if (response.statusCode == 200) {
      saveItemsCheck(response.body);
    }
  }

  saveItemsCheck(String idNumberCheck) async {
    Map data = {
      'idTypeCheck': _idTypeCheck,
    };
    var response = await http
        .post("https://intranet.prigo.com.mx/api/subdatachecksids", body: data);
    if (response.statusCode == 200) {
      int count = 0;
      int tamanio = 0;
      var dataSub = json.decode(response.body);
      tamanio = dataSub.length;
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      for (var x in dataSub) {
        count++;
        String typeSub = x['idSub'];
        String items =
            sharedPreferences.getString("valItemsCheck-" + x['idSub']) ??
                'no_data';
        String coments =
            sharedPreferences.getString("comentsAct-" + x['idSub']);
        String image = sharedPreferences.getString("nameImage-" + x['idSub']);

        saveItemsDetails(
            idNumberCheck, typeSub, items, coments, image, tamanio, count);
      }
    }
  }

  saveItemsDetails(String idNumberCheck, String typeSub, String its,
      String coments, String image, int tam, int count) async {
    Map item = {
      'idCheckList': idNumberCheck,
      'idSectionCheck': typeSub,
      'items': its,
      'comentariosCheck': coments,
      'nombreImagen': image
    };

    var responseItems = await http.post(
        "https://intranet.prigo.com.mx/api/guardaritemscheck",
        body: item);

    if (responseItems.statusCode == 200) {
      if (responseItems.body == "success") {
        if (count == tam) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) =>
                  ResultsCheckPage(_idTypeCheck, _totalPuntaje),
            ),
          );
        } else if (count != tam) {
          return Scaffold(
            body: Container(
                child: Center(
              child: Text("Guardando Datos..."),
            )),
          );
        }
      }
    }
  }

  saveInftoCheckList(String idSub, String valChecks, String nameImage) async {
    String comentsActu;
    if (_comentariosController.text == "") {
      setState(() {
        comentsActu = "Sin comentarios añadidos";
      });
    } else {
      setState(() {
        comentsActu = _comentariosController.text;
      });
    }

    if (idSub != null && comentsActu != null && _nameImageSuccess != "") {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString("valItemsCheck-" + idSub, valChecks);
      sharedPreferences.setString("comentsAct-" + idSub, comentsActu);
      sharedPreferences.setString("nameImage-" + idSub, _nameImageSuccess);
      saveInfoCheckDetail();
    } else if (_nameImageSuccess == "") {
      StatusAlert.show(
        context,
        blurPower: 10,
        titleOptions: StatusAlertTextConfiguration(
          style: TextStyle(
            fontFamily: 'Lato',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        duration: Duration(seconds: 2),
        title: "Envia la fotografía solicitada",
        configuration: IconConfiguration(icon: AntDesign.close),
      );
    }
  }

  saveInfoToSubActu(String idSub, String valChecks, String nameImage) async {
    String comentsActu;
    if (_comentariosController.text == "") {
      setState(() {
        comentsActu = "Sin comentarios añadidos";
      });
    } else {
      setState(() {
        comentsActu = _comentariosController.text;
      });
    }

    // if (idSub != null && comentsActu != null ) {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      sharedPreferences.setString("valItemsCheck-" + idSub, valChecks);
      sharedPreferences.setString("comentsAct-" + idSub, comentsActu);
      sharedPreferences.setString("nameImage-" + idSub, _nameImageSuccess);

      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => SubChecklistPage(_idTypeCheck, _idCheckActually,
              _sect, _numberPage + 1, _totalPages, _totalPuntaje),
        ),
      );
    // } else if (_nameImageSuccess == "") {
    //   StatusAlert.show(
    //     context,
    //     blurPower: 10,
    //     titleOptions: StatusAlertTextConfiguration(
    //       style: TextStyle(
    //         fontFamily: 'Lato',
    //         fontWeight: FontWeight.bold,
    //         fontSize: 14,
    //       ),
    //     ),
    //     duration: Duration(seconds: 2),
    //     title: "Envia la fotografía solicitada",
    //     configuration: IconConfiguration(icon: AntDesign.close),
    //   );
    // }
  }

  setIcon(int indicator) {
    if (indicator == 1) {
      return Icon(
        Icons.assistant_photo,
        size: 17,
        color: Colors.red,
      );
    } else if (indicator == 0) {
      return Icon(
        Icons.assistant_photo,
        size: 17,
        color: Colors.blueGrey[900],
      );
    }
  }

  Widget setButton(int numP, int limit) {
    Widget widget;
    if (numP < limit) {
      widget = IconButton(
          icon: Icon(Icons.arrow_forward),
          onPressed: () {
            saveInfoToSubActu(_sect, mapValues, _nameImageSuccess);
          });
    } else if (numP == limit) {
      widget = IconButton(
          icon: Icon(Icons.arrow_forward),
          onPressed: () {
            saveInftoCheckList(_sect, mapValues, _nameImageSuccess);
          });
    }
    return widget;
  }

  selectContainerImage(String status, File image, bool imgNecessary) {
    if (imgNecessary == true) {
      if (status == "noexecute" && image == null) {
        return Container(
          padding: EdgeInsets.only(top: 30),
          width: MediaQuery.of(context).size.width - 150,
          height: MediaQuery.of(context).size.width - 70,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: IconButton(
                    icon: Icon(Icons.add_a_photo, color: Colors.red),
                    onPressed: () {
                      _getImage(_image);
                    }),
              ),
              Center(
                child: Text(
                  "Tomar fotografía",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Lato',
                    color: Colors.black,
                  ),
                ),
              ),
              Container(
                child: Text(
                  _indicationsImg,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 14,
                    fontFamily: 'Lato',
                    fontWeight: FontWeight.bold,
                    color: Colors.black.withOpacity(0.6),
                  ),
                ),
              )
            ],
          ),
        );
      } else if (status == "pick_image" && image != null) {
        return Container(
          padding: EdgeInsets.only(top: 30),
          child: Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width - 150,
                height: MediaQuery.of(context).size.width - 70,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: PhotoView(
                    imageProvider: FileImage(_image, scale: 1),
                  ),
                ),
              ),
              Positioned(
                right: 5.0,
                bottom: 15.0,
                child: Column(
                  children: <Widget>[
                    IconButton(
                      iconSize: 40,
                      color: Colors.white,
                      icon: Icon(
                        Icons.check,
                        color: Colors.green,
                      ),
                      onPressed: () {
                        startUploadImage("Prueba 1", _image, "prueba");
                      },
                    ),
                    Text(
                      "Enviar",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.green,
                          fontFamily: 'Lato'),
                    )
                  ],
                ),
              ),
              Positioned(
                left: 5.0,
                bottom: 15.0,
                child: Column(
                  children: <Widget>[
                    IconButton(
                      iconSize: 40,
                      color: Colors.white,
                      icon: Icon(
                        Icons.replay,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        _getImage(_image);
                      },
                    ),
                    Text(
                      "Repetir",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.red,
                          fontFamily: 'Lato'),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      } else if (status == "loading_upload" && image != null) {
        return Container(
          padding: EdgeInsets.only(top: 30),
          width: MediaQuery.of(context).size.width - 150,
          height: MediaQuery.of(context).size.width - 70,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GlowingProgressIndicator(
                  child: Icon(
                    Icons.cloud_upload,
                    size: 30,
                  ),
                ),
                FadingText(
                  'Enviando fotografía...',
                  style: TextStyle(
                      fontFamily: 'Lato',
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
              ]),
        );
      } else if (status == "success_upload" && image != null) {
        return Container(
          width: MediaQuery.of(context).size.width - 150,
          height: MediaQuery.of(context).size.width - 70,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.check,
                  size: 30,
                  color: Colors.green,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Se envió correctamente la fotografía",
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                )
              ]),
        );
      } else if (status == "error_upload" && image != null) {
        return Container(
          width: MediaQuery.of(context).size.width - 150,
          height: MediaQuery.of(context).size.width - 70,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  iconSize: 40,
                  color: Colors.white,
                  icon: Icon(
                    Icons.replay,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    _getImage(_image);
                  },
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Error en el envío, intentalo de nuevo.",
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                )
              ]),
        );
      }
    } else if (_imgNecessary == false) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Fotografias no solicitadas.',
                style: TextStyle(
                    fontFamily: 'Lato',
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 5,
              ),
              Icon(Icons.notification_important, size: 26)
            ],
          ),
        ),
      );
    }
  }

  containerImage(ItemImage item, int index) {
    if (item.status == "noexecute" && item.image == null) {
      return Container(
        padding: EdgeInsets.only(top: 30),
        width: MediaQuery.of(context).size.width - 150,
        height: MediaQuery.of(context).size.width - 70,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: IconButton(
                  icon: Icon(Icons.add_a_photo, color: Colors.red),
                  onPressed: () {
                    getPhoto(item, index);
                  }),
            ),
            Center(
              child: Text(
                "Tomar fotografía",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Lato',
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              child: Text(
                'Fotografía / ' + item.indexName,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 14,
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.bold,
                  color: Colors.black.withOpacity(0.6),
                ),
              ),
            )
          ],
        ),
      );
    } else if (item.status == "pick_image" && item.image != null) {
      return Container(
          padding: EdgeInsets.only(top: 30),
          width: MediaQuery.of(context).size.width - 150,
          height: MediaQuery.of(context).size.width - 70,
          child: Container(
            height: MediaQuery.of(context).size.width - 70,
            child: Stack(
              fit: StackFit.loose,
              alignment: AlignmentDirectional.topCenter,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width - 70,
                  width: MediaQuery.of(context).size.width - 150,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: PhotoView(
                      imageProvider: FileImage(item.image, scale: 1),
                    ),
                  ),
                ),
                Positioned(
                  right: 55.0,
                  bottom: 15.0,
                  child: Column(
                    children: <Widget>[
                      IconButton(
                        iconSize: 40,
                        color: Colors.white,
                        icon: Icon(
                          Icons.check,
                          color: Colors.green,
                        ),
                        onPressed: () {
                          uploadFile(item, index);
                        },
                      ),
                      Text(
                        "Enviar",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.green,
                            fontFamily: 'Lato'),
                      )
                    ],
                  ),
                ),
                Positioned(
                  left: 55.0,
                  bottom: 15.0,
                  child: Column(
                    children: <Widget>[
                      IconButton(
                        iconSize: 40,
                        color: Colors.white,
                        icon: Icon(
                          Icons.replay,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          getPhoto(item, index);
                        },
                      ),
                      Text(
                        "Repetir",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.red,
                            fontFamily: 'Lato'),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ));
    } else if (item.status == "loading_upload" && item.image != null) {
      return Container(
        padding: EdgeInsets.only(top: 30),
        width: MediaQuery.of(context).size.width - 150,
        height: MediaQuery.of(context).size.width - 70,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GlowingProgressIndicator(
                child: Icon(
                  Icons.cloud_upload,
                  size: 30,
                ),
              ),
              FadingText(
                'Enviando fotografía...',
                style: TextStyle(
                    fontFamily: 'Lato',
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
            ]),
      );
    } else if (item.status == "success_upload" && item.image != null) {
      return Container(
        width: MediaQuery.of(context).size.width - 150,
        height: MediaQuery.of(context).size.width - 70,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.check,
                size: 30,
                color: Colors.green,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Se envió correctamente la fotografía",
                style: TextStyle(
                    fontFamily: 'Lato',
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              )
            ]),
      );
    }
  }

  containerPhotosToCheck(bool imgNecessary) {
    if (imgNecessary == true) {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 200,
        child: Container(
          padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 20),
          color: Color(0Xf0f0f0),
          width: MediaQuery.of(context).size.width,
          child: Container(
            child: ListView(
              physics: ScrollPhysics(),
              children: <Widget>[
                Container(
                  child: Center(
                    child: Text(
                      _indicationsImg,
                      style: TextStyle(
                          fontFamily: 'Lato', fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.only(top: 10, bottom: 20),
                    scrollDirection: Axis.vertical,
                    itemCount: imagesListTotal.length,
                    itemBuilder: (BuildContext context, int index) {
                      return containerImage(imagesListTotal[index], index);
                    }),
              ],
            ),
          ),
        ),
      );
    } else if (_imgNecessary == false) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Fotografias no solicitadas.',
                style: TextStyle(
                    fontFamily: 'Lato',
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 5,
              ),
              Icon(Icons.notification_important, size: 26)
            ],
          ),
        ),
      );
    }
  }

  selectContainer(int index) {
    if (index == 0) {
      return Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        child: Card(
          elevation: 10,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Column(children: <Widget>[
            Expanded(
              child: checkList(_sect),
            )
          ]),
        ),
      );
    } else if (index == 1) {
      return Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                containerPhotosToCheck(_imgNecessary),
              ]),
        ),
      );
    } else if (index == 2) {
      return Container(
        padding: EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Center(
              child: Container(
            padding: EdgeInsets.all(20),
            child: SizedBox(
              height: MediaQuery.of(context).size.height / 2,
              child: TextFormField(
                controller: _comentariosController,
                style:
                    TextStyle(fontFamily: 'Lato', fontWeight: FontWeight.bold),
                keyboardType: TextInputType.multiline,
                maxLines: 10,
                decoration: InputDecoration(
                    labelStyle: TextStyle(
                        fontFamily: 'Lato',
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                    labelText: 'Comentarios: ',
                    contentPadding: const EdgeInsets.all(10.0)),
              ),
            ),
          )),
        ),
      );
    }
  }

  Widget checkList(String _sect) {
    Widget w;
    if (_sect == '') {
      w = Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SpinKitCircle(
              color: Colors.black,
              size: 40.0,
            ),
            JumpingText(
              'Cargando...',
              style: TextStyle(
                  fontFamily: 'Lato',
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 12),
            ),
          ],
        ),
      );
    } else {
      w = ListView.builder(
          padding: EdgeInsets.only(bottom: 10, top: 10),
          itemCount: items.length,
          itemBuilder: (BuildContext context, int index) {
            return CheckboxListTile(
              onChanged: (bool val) {
                if (val == true) {
                  setState(() {
                    _totalPuntaje += items[index].puntaje;
                  });
                } else if (val == false) {
                  setState(() {
                    _totalPuntaje -= items[index].puntaje;
                  });
                }

                setState(() {
                  valCheck.update('"' + items[index].index.toString() + '"',
                      (existingValue) => val);
                  mapValues = json.encode(valCheck.toString());
                  itemsStatus[index] = !itemsStatus[index];
                });
              },
              value: itemsStatus[index],
              dense: true,
              controlAffinity: ListTileControlAffinity.trailing,
              secondary: setIcon(items[index].critico),
              title: Container(
                child: Text(
                  items[index].descripcion,
                  style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 14.5,
                      fontWeight: FontWeight.w800,
                      color: Colors.black),
                ),
              ),
            );
          });
    }

    return w;
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _numberPage = widget.numberPage;
      _totalPages = widget.totalPages;
      _idTypeCheck = widget.idTypeCheck;
      _totalPuntaje = widget.puntajeAct;
      _idCheckActually = widget.idCheckActually;
    });
    this.getDataSubs(_idTypeCheck, _numberPage, _totalPages);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: FFNavigationBar(
        theme: FFNavigationBarTheme(
            barHeight: 60,
            barBackgroundColor: Colors.blueGrey[900],
            selectedItemBackgroundColor: Colors.blueGrey[900],
            selectedItemIconColor: Colors.white,
            selectedItemLabelColor: Colors.black,
            unselectedItemTextStyle:
                TextStyle(fontFamily: 'Lato', fontSize: 14),
            selectedItemTextStyle:
                TextStyle(letterSpacing: 0, fontFamily: 'Lato', fontSize: 14),
            unselectedItemLabelColor: Colors.white38,
            unselectedItemIconColor: Colors.white38),
        selectedIndex: selectedIndex,
        onSelectTab: (index) {
          setState(() {
            selectedIndex = index;
          });
        },
        items: [
          FFNavigationBarItem(
            iconData: Icons.list,
            label: 'Checklist',
            selectedLabelColor: Colors.white,
          ),
          FFNavigationBarItem(
            iconData: Icons.camera_enhance,
            label: 'Fotografías',
            selectedLabelColor: Colors.white,
          ),
          FFNavigationBarItem(
            iconData: Icons.line_style,
            label: 'Comentarios',
            selectedLabelColor: Colors.white,
          ),
        ],
      ),
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.blueGrey[900],
      appBar: AppBar(
        actions: <Widget>[setButton(_numberPage, _totalPages)],
        centerTitle: true,
        title: Text(
          _nombre,
          style: TextStyle(
              fontFamily: 'Lato',
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: Colors.white),
        ),
      ),
      body: selectContainer(selectedIndex),
    );
  }
}

class SubCheck {
  String idSub;
  SubCheck(this.idSub);
}

class SubCheckList {
  final String nombre;
  SubCheckList(this.nombre);
}

class Item {
  final int index;
  final String descripcion;
  final int critico;
  final int puntaje;
  final String status;

  Item(this.index, this.descripcion, this.critico, this.puntaje, this.status);
}

class ItemImage {
  final String indexName;
  final File image;
  final String image64;
  final String status;

  ItemImage({this.indexName, this.image, this.image64, this.status});
}
